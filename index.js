/* eslint-disable no-console */
const Koa = require('koa');
const path = require('path');
const serve = require('koa-static');
const route = require('koa-route');
const bodyParser = require('koa-bodyparser');
const nameDictsServer = require('./src/Service/nameDict.js');
const searchChannel = require('./src/Control/searchChannel.js');
const channelDetail = require('./src/Control/channelDetail.js');
const playListItem = require('./src/Control/playListitem.js');
const videoList = require('./src/Control/videoList.js');
const dbmov = require('./src/dbMove.js')
// 开始
const app = new Koa();
const main = serve(path.join(`${__dirname}/public`));

app.use(bodyParser());
app.use(main);

// 添加 字典
/*
 直接通过channelURL了 搜索活着keyword url 格式要对
{
    keyword: null,
    channelUrl: null,
    channelId: null,
    name: null,
    tags: "StreetAddress,Ueberseeallee 1,PhoneNO.,NaN,Country,Germany city,Hamburg",
    level: 2,
    platform: "youtube",
    email: "oschirmer@computerbild.de",
    fansSum: ""
}
*/
app.use(route.post('/addDicts', nameDictsServer.naemDictInster));
// 启动搜索
app.use(route.get('/searchChannelStart', searchChannel.start));
// 取消搜索任务
app.use(route.get('/searchChannelCancel', searchChannel.cancel));

// 启动channelDetail
app.use(route.get('/channelDetailStart', channelDetail.start));
// 取消channelDetail
app.use(route.get('/channelDetailCancel', channelDetail.cancel));

// // 启动playListItem
app.use(route.get('/playListItemStart', playListItem.start));
// // 取消playListItem
app.use(route.get('/playListItemCancel', playListItem.cancel));

// 启动videoList
app.use(route.get('/videoListStart', videoList.start));
// 取消videoList
app.use(route.get('/videoListCancel', videoList.cancel));

app.use(route.get('/dbmove', dbmov.dbmove))

app.use(route.get('/'));

app.listen(39005);
console.log('listen in http:/0.0.0.0:39005');
