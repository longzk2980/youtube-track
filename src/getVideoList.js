const Crawler = require('crawler');
const axios = require('axios');
const db = require('./utill/db.js');

const dburl = 'mongodb://127.0.0.1:27017/';


const c = new Crawler({
  // 在每个请求处理完毕后将调用此回调函数
  headers:
    {
      'cache-control': 'no-cache',
      Connection: 'keep-alive',
      'Accept-Encoding': 'gzip, deflate',
      Host: 'www.googleapis.com',
      'Postman-Token': '837d8736-6132-4405-a75d-4ad7a1fe3113,4093fec6-2ad6-43fb-a38d-cadb9aa1aad9',
      'Cache-Control': 'no-cache',
      Accept: '*/*',
      'User-Agent': 'PostmanRuntime/7.17.1',
    },
  jQuery: false,
  callback (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            try {
                const data = JSON.parse(res.body).items[0]
                if (data) {
                    data.channelId = data.id.channelId
                    data.status = 'false'
                    updateData(data, res.options.name)
                }   
            } catch (error) {
                console.log(error)
            }
        }
        done();
    },
});


const flag = 0;
async function updateData(data, name) {
  db.upsert(dburl, 'youtubeSpider', 'ChannelID', { channelId: data.channelId }, { $set: data }, { upsert: true }, (err, result) => {
    if (err) {
      console.log('数据修改失败！');
      return;
    }
        console.log(flag, '成功');
    updateDicts(name);
  });
}
async function updateDicts(name) {
  db.upsert(dburl, 'youtubeSpider', 'nameDicts', { name: name }, { $set: { name: name, taskstatus: 'success' } }, { upsert: true }, (err, result) => {
    if (err) {
      console.log('数据修改失败！');
      return;
    }
        console.log('成功');
  });
}

const findDicts = () => new Promise((resolve, reject) => {
    db.find(dburl, 'youtubeSpider', 'nameDicts', { taskstatus: 'false' }, (err, result) => {
      if (err) {
        console.log('数据查找失败！');
        db.close();
        return;
      }
            resolve(result);
    });
  });

const key = 'AIzaSyBc3xAnnqvIEh3pbdBQTm97Lpm7Ae29EEk';
module.exports.searchChannel = async () => {
  const res = await findDicts();
  const ruls = [];
  res.forEach((el) => {
    const url = {
      url: `https://www.googleapis.com/youtube/v3/search?part=id,snippet&key=${key}&type=channel&q=${encodeURIComponent(el.name)}&maxResults=1&order=viewCount`,
      rateLimit: Math.random(),
      name: el.name,
    };
    ruls.push(url);
  });
  console.log(ruls);
  c.queue(ruls);
};

module.exports.channelId = async (id) => {
  console.log(id);
  // c.queue([{
  //     url:`https://www.googleapis.com/youtube/v3/search?part=id,snippet&key=${key}&type=channel&q=${encodeURIComponent(el.name)}&maxResults=1&order=viewCount`,
  //     rateLimit: Math.random()
  // }])
}
;