// 引入数据库操作
const db = require('../utill/db.js');
const logger = require('../utill/logger.js');

const DBconfig = require('../utill/apikey.js');

const dbUrl = DBconfig.AWS_DB;
const dbName = 'youtubeSpider';
const dbCol = 'timingchannel';

function toString(obj) {
  return JSON.stringify(obj);
}

//  修改状态
function timingChangeStatus(findjson, updatejson) {
  return new Promise((resolve, reject) => {
    db.upsert(dbUrl, dbName, dbCol, findjson, updatejson, { multi: true, upsert: false }, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库修改出错:`);
        logger.error(`findjson:${toString(findjson)}------updatejson:${toString(updatejson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(findjson)}:数据库修成功`);
      resolve();
    });
  });
}

//  查询一个
function timingchannelFindOne(findjson) {
  return new Promise((resolve, reject) => {
    db.find(dbUrl, dbName, dbCol, findjson,
      { pageamount: 1, page: 0, sort: { level: -1 } }, (err, result) => {
        if (err) {
          logger.error(`${dbUrl}--${dbName}--${dbCol}数据库查询出错:`);
          logger.error(`findjson:${toString(findjson)}---排序：{ pageamount: 1, page: 0, sort: { level: 1 } }`);
          logger.error(err);
          reject();
          return;
        }
        logger.info(`${toString(result)}:查询成功`);
        resolve(result);
      });
  });
}

// 如果有就更新整个 没有就插入
function timingchannelInster(insterjson) {
  return new Promise((resolve, reject) => {
    const findjson = {
      channelId: insterjson.channelId,
      channelUrl: insterjson.channelUrl,
      keyword: insterjson.keyword,
      name: insterjson.name,
    };
    db.upsert(dbUrl, dbName, dbCol, findjson, { $set: insterjson }, { upsert: true }, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库连接出错:`);
        logger.error(`insterjson:${toString(insterjson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(insterjson)}:数据插入成功`);
      resolve();
    });
  });
}

//  修改将查找到的数据taskstatus 值都改为 success
module.exports.timingChangeStatus = async (findjson) => {
  await timingChangeStatus(findjson, { $set: { taskstatus: 'success' } });
};

// 查找一个 taskstatus = false 安装level 排序
module.exports.timingchannelFindOne = async () => {
  try {
    const result = await timingchannelFindOne({ taskstatus: 'false' });
    return result;
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// 插入字典 inster 对象 一个字典对象
module.exports.timingchannelInster = async (insterjson) => {
  await timingchannelInster(insterjson);
};
