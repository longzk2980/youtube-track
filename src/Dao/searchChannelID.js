// TODO:操作 ChannelID

//  插入，存储  同时修改所有查询到的状态  查询出50条
// 引入数据库操作
const db = require('../utill/db.js');
const logger = require('../utill/logger.js');
const DBconfig = require('../utill/apikey.js');

const dbUrl = DBconfig.AWS_DB;
const dbName = 'youtubeSpider';
const dbCol = 'channelID';

function toString(obj) {
  return JSON.stringify(obj);
}

//  修改状态
function channelIDChangeStatus(findjson, updatejson) {
  return new Promise((resolve, reject) => {
    db.upsert(dbUrl, dbName, dbCol, findjson, updatejson, { multi: true, upsert: false }, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库修改出错:`);
        logger.error(`findjson:${toString(findjson)}------updatejson:${toString(updatejson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(findjson)}:数据库修成功`);
      resolve();
    });
  });
}

//  查询一个
function channelIDFind50(findjson) {
  return new Promise((resolve, reject) => {
    db.find(dbUrl, dbName, dbCol, findjson,
      { pageamount: 50, page: 0, sort: { level: -1 } }, (err, result) => {
        if (err) {
          logger.error(`${dbUrl}--${dbName}--${dbCol}数据库查询出错:`);
          logger.error(`findjson:${toString(findjson)}---排序：{ pageamount: 1, page: 0, sort: { level: 1 } }`);
          logger.error(err);
          reject();
          return;
        }
        logger.info(` channelID 查询成功结果有${result.length}条`);
        resolve(result);
      });
  });
}

// 如果有就更新整个 没有就插入
function channelIDInster(insterjson) {
  return new Promise((resolve, reject) => {
    const findjson = {
      channelId: insterjson.channelId,
    };
    db.upsert(dbUrl, dbName, dbCol, findjson, { $set: insterjson }, { upsert: true }, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库连接出错:`);
        logger.error(`insterjson:${toString(insterjson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(findjson)}:数据插入成功`);
      resolve();
    });
  });
}

//  修改将查找到的数据taskstatus 值都改为 success
module.exports.channelIDChangeStatus = async (findjson) => {
  await channelIDChangeStatus(findjson, { $set: { taskstatus: 'success', level: 0 } });
};

// 查找一个 taskstatus = false 安装level 排序
module.exports.channelIDFind50 = async () => {
  try {
    const result = await channelIDFind50({ taskstatus: 'false' });
    return result;
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// 插入字典 inster 对象 一个字典对象
module.exports.channelIDInster = async (insterjson) => {
  await channelIDInster(insterjson);
};
