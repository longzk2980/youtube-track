
// 引入数据库操作
const db = require('../utill/db.js');
const logger = require('../utill/logger.js');
const DBconfig = require('../utill/apikey.js');

const dbUrl = DBconfig.AWS_DB;
const dbName = 'youtubeSpider';
const dbCol = 'videoList';

function toString(obj) {
  return JSON.stringify(obj);
}

// 查询插入完成后将拉取到到数据更新channlID 表
function ChangeStatus(findjson, updatejson) {
  return new Promise((resolve, reject) => {
    db.upsert(dbUrl, dbName, dbCol, findjson, updatejson, { multi: true, upsert: false }, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库修改出错:`);
        logger.error(`findjson:${toString(findjson)}------updatejson:${toString(updatejson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(findjson)}:数据库修成功`);
      resolve();
    });
  });
}

// 查询出 50个一批
function Find50(findjson) {
  return new Promise((resolve, reject) => {
    db.find(dbUrl, dbName, dbCol, findjson,
      { pageamount: 50, page: 0, sort: { spUtcDate: -1 } }, (err, result) => {
        if (err) {
          logger.error(`${dbUrl}--${dbName}--${dbCol}数据库查询出错:`);
          logger.error(`findjson:${toString(findjson)}---排序：{ pageamount: 1, page: 0, sort: { spUtcDate: 1 } }`);
          logger.error(err);
          reject();
          return;
        }
        logger.info(`${toString(result.length)}:查询成功`);
        resolve(result);
      });
  });
}

// 查询出 通过ID查询一批
function FindById(findjson) {
  return new Promise((resolve, reject) => {
    db.find(dbUrl, dbName, dbCol, findjson,
      { pageamount: 600, page: 0, sort: { spUtcDate: -1 } }, (err, result) => {
        if (err) {
          logger.error(`${dbUrl}--${dbName}--${dbCol}数据库查询出错:`);
          logger.error(`findjson:${toString(findjson)}---排序：{ pageamount: 1, page: 0, sort: { spUtcDate: 1 } }`);
          logger.error(err);
          reject();
          return;
        }
        logger.info(`${toString(result)}:查询成功`);
        resolve(result);
      });
  });
}

// 如果有就更新整个 没有就插入
function InsterMany(insterjson) {
  return new Promise((resolve, reject) => {
    db.insertMany(dbUrl, dbName, dbCol, insterjson, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库连接出错:`);
        logger.error(`videoList insterjson:${toString(insterjson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(insterjson.length)}:video数据插入成功`);
      resolve();
    });
  });
}

//  修改将查找到的数据taskstatus 值都改为 success
module.exports.ChangeStatus = async (findjson) => {
  await ChangeStatus(findjson, { $set: { taskstatus: 'success' } });
};

// 查找一个 taskstatus = false 安装level 排序
module.exports.Find50 = async () => {
  try {
    const result = await Find50({ taskstatus: 'false' });
    return result;
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// 查找一个 id  排序
module.exports.FindById = async (channelID) => {
  try {
    const result = await FindById({ channelId: channelID });
    return result;
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// 插入字典 inster 对象 一个字典对象
module.exports.InsterMany = async (insterjson) => {
  await InsterMany(insterjson);
};
