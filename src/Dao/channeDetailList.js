
// 引入数据库操作
const db = require('../utill/db.js');
const logger = require('../utill/logger.js');
const DBconfig = require('../utill/apikey.js');

const dbUrl = DBconfig.AWS_DB;
const dbName = 'youtubeSpider';
const dbCol = 'channelsList';

function toString(obj) {
  return JSON.stringify(obj);
}

//  修改状态
function ChangeStatus(findjson, updatejson) {
  return new Promise((resolve, reject) => {
    db.upsert(dbUrl, dbName, dbCol, findjson, updatejson, { multi: true, upsert: false }, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库修改出错:`);
        logger.error(`findjson:${toString(findjson)}------updatejson:${toString(updatejson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`${toString(findjson)}:数据库修成功`);
      resolve();
    });
  });
}

//  查询一个
function FindOne(findjson) {
  return new Promise((resolve, reject) => {
    db.find(dbUrl, dbName, dbCol, findjson,
      { pageamount: 1, page: 0, sort: { level: -1 } }, (err, result) => {
        if (err) {
          logger.error(`${dbUrl}--${dbName}--${dbCol}数据库查询出错:`);
          logger.error(`findjson:${toString(findjson)}---排序：{ pageamount: 1, page: 0, sort: { spUtcDate: -1 } }`);
          logger.error(err);
          reject();
          return;
        }
        logger.info('channelList视频上传列表查询成功');
        resolve(result);
      });
  });
}

// 如果有就更新整个 没有就插入
function InsterMany(insterjson) {
  return new Promise((resolve, reject) => {
    db.insertMany(dbUrl, dbName, dbCol, insterjson, (err) => {
      if (err) {
        logger.error(`${dbUrl}--${dbName}--${dbCol}数据库连接出错:`);
        logger.error(`insterjson:${toString(insterjson)}`);
        logger.error(err);
        reject();
        return;
      }
      logger.info(`channelList 数据插入成功${insterjson.length} 条`);
      resolve();
    });
  });
}

//  修改将查找到的数据taskstatus 值都改为 success
module.exports.ChangeStatus = async (findjson) => {
  // { 'contentDetails.relatedPlaylists.uploads': Id ,taskstatus:false}
  await ChangeStatus(findjson, { $set: { taskstatus: 'success' } });
};

// 查找一个 taskstatus = false 安装level 排序
module.exports.FindOne = async () => {
  try {
    const result = await FindOne({ taskstatus: 'false', 'contentDetails.relatedPlaylists.uploads': { $ne: '' } });
    return result;
  } catch (error) {
    logger.error(error);
    return false;
  }
};

// 插入字典 inster 对象 一个字典对象
module.exports.InsterMany = async (insterjson) => {
  await InsterMany(insterjson);
};
// 查找功能
module.exports.Find = async (findjson) => {
  try {
    const result = await FindOne(findjson);
    return result;
  } catch (error) {
    logger.error(error);
    return false;
  }
};
