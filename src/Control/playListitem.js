const schedule = require('node-schedule');
const logger = require('../utill/logger.js');
const apiKey = require('../utill/apikey.js');
const API = require('../api.js');
const channelListServer = require('../Service/channelDetaiList.js');
const playListItemServer = require('../Service/playListitem.js');

let JOB = null;
let SEARCH_LEVEL = 0;

function toString(obj) {
  return JSON.stringify(obj);
}
// 返回APIkey
function getKey() {
  return apiKey.getCurrentKey(2);
}

// 查询插入完成后将拉取到到数据更新channlID 表
async function updatePlaylistId(Id) {
  await channelListServer.channelListChangeStatus({ taskstatus: 'false', 'contentDetails.relatedPlaylists.uploads': Id });
}

// 将拉到到数据插入到channelList
async function insertPlayItemsList(list, Id) {
  try {
    await playListItemServer.playListInsterMany(list, SEARCH_LEVEL);
    await updatePlaylistId(Id);
  } catch (error) {
    logger.error(`插入和更新出错了${toString(list)}`);
  }
}

// 对返回到数据做检查和清理出错到话及时拦截停止
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data && data.length > 0) {
    return data;
  }
  return false;
}

// 通过 idList 发起请求
function requestApi(Id, pagetoken) {
  return new Promise((resolve, reject) => {
    const key = getKey();
    API.playListItem(Id, key, pagetoken).then((res) => {
      const playlist = dataCheck(res);
      if (playlist) {
        insertPlayItemsList(playlist, Id);
      } else if (playlist === false) {
        updatePlaylistId(Id);
      }
      resolve(res);
    }).catch(() => {
      reject();
    });
  });
}

// 通过page token 获取视频列表
let PAGE_SUM = 0;
async function pageRequest(id, pagetoken) {
  const res = await requestApi(id, pagetoken);
  const nextRes = JSON.parse(res);
  if (nextRes.nextPageToken && PAGE_SUM < 7) {
    // 有下一页就取消定时任务 执行页面请求的回调，一直到没有next page在重启任务
    await cancel();
    logger.info(`下一页pagetoken:${nextRes.nextPageToken}`);
    setTimeout(() => {
      logger.info(`下一页执行,第 ${PAGE_SUM}页`);
      PAGE_SUM += 1;
      pageRequest(id, nextRes.nextPageToken);
    }, 1000 * 80);
  } else {
    PAGE_SUM = 0;
    logger.warn(`请求没有token: 第${PAGE_SUM}页`);
    await scdule();
  }
}

// 获取 Playid 列表 好变成字符串 然后放到 请求里面
function uploadUrl(channelDetail) {
  if (channelDetail.contentDetails.relatedPlaylists.uploads) {
    logger.info(`uplistId:${channelDetail.contentDetails.relatedPlaylists.uploads};====id:${channelDetail.id}======channelId:${channelDetail.channelId}`);
    SEARCH_LEVEL = channelDetail.level || 0;
    return channelDetail.contentDetails.relatedPlaylists.uploads;
  }
  return false;
}

// 任务入口
const startJob = async () => {
  const channelDetail = await channelListServer.channelListFindOne(); // 如果查询不到会
  if (channelDetail) {
    const uplads = uploadUrl(channelDetail);
    await pageRequest(uplads, '');
  } else {
    await cancel();
  }
};

// 取消任务
const cancel = async (ctx, next) => {
  if (JOB) {
    JOB.cancel();
    logger.info(`PlayListitem任务已经取消：${JOB}`);
    JOB = null;
    if (ctx) {
      ctx.body = { success: `PlayListitem任务取消-JOB：${JOB}` };
      await next();
    }
  } else {
    logger.error(`PlayListitem任务还未开始-JOB：${JOB}`);
    if (ctx) {
      ctx.body = { error: `PlayListitem任务还未开始-JOB：${JOB}` };
      await next();
    }
  }
};
// 启动任务
const scdule = async (ctx, next) => {
  if (!JOB && PAGE_SUM === 0) {
    const rule = new schedule.RecurrenceRule();
    rule.second = 40;
    logger.info(`PlayListitem任务启动-second：${rule.second}`);
    JOB = schedule.scheduleJob(rule, () => {
      startJob();
    });
    if (ctx) {
      ctx.body = { success: `PlayListitem任务启动-second：${rule.second}` };
      await next();
    }
  } else {
    if (ctx) {
      ctx.body = { error: 'PlayListitem schedule is alerady exits' };
      await next();
    }
    logger.error('PlayListitem schedule is alerady exits');
  }
};

module.exports.cancel = async (ctx, next) => {
  await cancel(ctx, next);
};

module.exports.start = async (ctx, next) => {
  await scdule(ctx, next);
};
