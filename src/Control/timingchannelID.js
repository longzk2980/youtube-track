const schedule = require('node-schedule');
const logger = require('../utill/logger.js');
const API = require('../api');
const timmingchannelDao = require('../Dao/timingchannelID.js');
const channelListServer = require('../Service/channelDetaiList.js');
const videoListServer = require('../Service/videoList.js');

let JOB = null;

function toString(obj) {
  return JSON.stringify(obj);
}

// 数据检查
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data && data.length > 0) {
    return data[0];
  }
  return false;
}

// 修改channelID 表
function updateChannelId(item) {
  timmingchannelDao.timingChangeStatus({ channelId: item.channelId, taskstatus: 'false' }).catch(() => {
    logger.error(`修改${toString(item)} 出错`);
  });
}

// 通过 idList 发起请求
async function Datacheck(Idlist) {
  const item = dataCheck(Idlist);
  const Detail = await channelListServer.Find({ channelId: item.channelId });
  const Video = await videoListServer.VideoFindById(item.channelId);
  if (Detail && Video) {
    try {
      await API.sendChannel(Detail);
      await API.sendVideo(Video);
      updateChannelId(item);
    } catch (error) {
      logger.error(error);
    }
  }
}

// 任务入口
const startJob = async () => {
  const IdList = await timmingchannelDao.timingchannelFindOne(); // 如果查询不到会
  if (IdList) {
    Datacheck(IdList);
  } else {
    //   如果查询不到的话还是继续
    // await cancel();
  }
};

// 取消任务
const cancel = async (ctx, next) => {
  if (JOB) {
    JOB.cancel();
    logger.info(`timmingchannel任务已经取消：${JOB}`);
    JOB = null;
    if (ctx) {
      ctx.body = { success: `timmingchannel任务取消-JOB：${JOB}` };
      await next();
    }
  } else {
    logger.error(`timmingchannel任务还未开始-JOB：${JOB}`);
    if (ctx) {
      ctx.body = { error: `timmingchannel任务还未开始-JOB：${JOB}` };
      await next();
    }
  }
};
// 启动任务
const scdule = async (ctx, next) => {
  if (!JOB) {
    const rule = new schedule.RecurrenceRule();
    // rule.hour = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22];
    rule.second = 20;
    logger.info(`timmingchannel任务启动-second：${rule.second}`);
    JOB = schedule.scheduleJob(rule, () => {
      startJob();
    });
    ctx.body = { success: `timmingchannel任务启动-second：${rule.second}` };
    await next();
  } else {
    ctx.body = { error: 'timmingchannel schedule is alerady exits' };
    logger.error('timmingchannel schedule is alerady exits');
    await next();
  }
};

module.exports.cancel = async (ctx, next) => {
  await cancel(ctx, next);
};

module.exports.start = async (ctx, next) => {
  await scdule(ctx, next);
};
