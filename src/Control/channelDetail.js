const schedule = require('node-schedule');
const logger = require('../utill/logger.js');
const apiKey = require('../utill/apikey');
const API = require('../api');
const channelIDServer = require('../Service/searchChannel.js');
const channelListServer = require('../Service/channelDetaiList.js');

let JOB = null;

function toString(obj) {
  return JSON.stringify(obj);
}

// 返回APIkey
function getKey() {
  return apiKey.getCurrentKey(1);
}

// 数据检查
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data && data.length > 0) {
    return data;
  }
  return false;
}

// 修改channelID 表
function updateChannelId(Channellist) {
  Channellist.forEach((item) => {
    channelIDServer.channelIDChangeStatus({ channelId: item.id, taskstatus: 'false' }).catch(() => {
      logger.error(`修改${toString(item)} 出错`);
    });
  });
}
// 插入channellist
async function insterData(Channellist, list) {
  try {
    await channelListServer.channelListInsterMany(Channellist, list);
    updateChannelId(Channellist);
  } catch (error) {
    logger.error(`修改${toString(list)} 出错`);
  }
}

// 通过 idList 发起请求
function requestApi(List) {
  const key = getKey();
  const channelIDList = [];
  List.forEach((e) => {
    if (e.channelId) {
      channelIDList.push(e.channelId);
    }
  });
  API.channels(channelIDList.toString(), key).then((res) => {
    const item = dataCheck(res);
    if (item) {
      insterData(item, List);
    } else {
      updateChannelId(item);
    }
  }).catch(() => {
  });
}

// 任务入口
const startJob = async () => {
  const List = await channelIDServer.channelIDFind50(); // 如果查询不到会
  if (List) {
    requestApi(List);
  } else {
    await cancel();
  }
};

// 取消任务
const cancel = async (ctx, next) => {
  if (JOB) {
    JOB.cancel();
    logger.info(`channelList任务已经取消：${JOB}`);
    JOB = null;
    if (ctx) {
      ctx.body = { success: `channelList任务取消-JOB：${JOB}` };
      await next();
    }
  } else {
    logger.error(`channelList任务还未开始-JOB：${JOB}`);
    if (ctx) {
      ctx.body = { error: `channelList任务还未开始-JOB：${JOB}` };
      await next();
    }
  }
};
// 启动任务
const scdule = async (ctx, next) => {
  if (!JOB) {
    const rule = new schedule.RecurrenceRule();
    rule.second = 20;
    logger.info(`channelList任务启动-second：${rule.second}`);
    JOB = schedule.scheduleJob(rule, () => {
      startJob();
    });
    ctx.body = { success: `channelList任务启动-second：${rule.second}` };
    await next();
  } else {
    ctx.body = { error: 'channelList schedule is alerady exits' };
    logger.error('channelList schedule is alerady exits');
    await next();
  }
};

module.exports.cancel = async (ctx, next) => {
  await cancel(ctx, next);
};

module.exports.start = async (ctx, next) => {
  await scdule(ctx, next);
};
