const schedule = require('node-schedule');
const logger = require('../utill/logger.js');
const apiKey = require('../utill/apikey');
const API = require('../api');
const playListItemServer = require('../Service/playListitem.js');
const videoListServer = require('../Service/videoList');

let JOB = null;

function toString(obj) {
  return JSON.stringify(obj);
}
// 返回APIkey
function getKey() {
  return apiKey.getCurrentKey(3);
}

// 查询插入完成后将拉取到到数据更新channlID 表
function updatePlaylistId(list) {
  list.forEach((item) => {
    playListItemServer.playListChangeStatus({ 'contentDetails.videoId': item.id }).catch(() => {
      logger.error(`修改${toString(item)} 出错`);
    });
  });
}

// 将拉到到数据插入到channelList
async function insterVideoData(list) {
  try {
    await videoListServer.VideoInsterMany(list);
    updatePlaylistId(list);
  } catch (error) {
    logger.error(`插入和更新出错了${toString(list)}`);
  }
}

// 对返回到数据做检查和清理出错到话及时拦截停止
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data && data.length > 0) {
    return data;
  }
  return false;
}

// 通过 videolist 发起请求
function requestApi(Idlist) {
  const key = getKey();
  API.videoList(Idlist.toString(), key).then((res) => {
    const item = dataCheck(res);
    if (item) {
      insterVideoData(item);
    } else {
      updatePlaylistId(item);
    }
  }).catch(() => {
  });
}
// 任务入口
const startJob = async () => {
  const Videolist = await playListItemServer.VideoIDFind50(); // 如果查询不到会
  if (Videolist) {
    requestApi(Videolist);
  } else {
    await cancel();
  }
};

// 取消任务
const cancel = async (ctx, next) => {
  if (JOB) {
    JOB.cancel();
    logger.info(`video任务已经取消：${JOB}`);
    JOB = null;
    if (ctx) {
      ctx.body = { success: `video任务取消-JOB：${JOB}` };
      await next();
    }
  } else {
    logger.error(`video任务还未开始-JOB：${JOB}`);
    if (ctx) {
      ctx.body = { error: `video任务还未开始-JOB：${JOB}` };
      await next();
    }
  }
};
// 启动任务
const scdule = async (ctx, next) => {
  if (!JOB) {
    const rule = new schedule.RecurrenceRule();
    rule.second = 50;
    logger.info(`video任务启动-second：${rule.second}`);
    JOB = schedule.scheduleJob(rule, () => {
      startJob();
    });
    ctx.body = { success: `video任务启动-second：${rule.second}` };
    await next();
  } else {
    ctx.body = { error: 'videoschedule is alerady exits' };
    logger.error('videoschedule is alerady exits');
    await next();
  }
};

module.exports.cancel = async (ctx, next) => {
  await cancel(ctx, next);
};

module.exports.start = async (ctx, next) => {
  await scdule(ctx, next);
};
