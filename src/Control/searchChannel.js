
const schedule = require('node-schedule');
const logger = require('../utill/logger.js');
const nameDictsServer = require('../Service/nameDict.js');
const apiKey = require('../utill/apikey');
const API = require('../api');
const channelIDServer = require('../Service/searchChannel.js');

let JOB = null;

function toString(obj) {
  return JSON.stringify(obj);
}
// 返回APIkey
function getKey() {
  return apiKey.getCurrentKey(0);
}
// 数据检查
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data.length > 0) {
    const item = data[0];
    return item;
  }
  return false;
}
// 修改字典
async function updateDicts(name, searchresult) {
  try {
    const findjson = {};
    findjson[name.type] = name[name.type];
    findjson.taskstatus = 'false';
    await nameDictsServer.naemDictChangeStatus(findjson, searchresult);
  } catch (error) {
    logger.error(`修改${toString(name)} 出错`);
  }
}
// 插入
async function updateData(data, name) {
  try {
    const temp = { ...data, ...{ level: 1 } };
    temp.level = name.level || 1;
    await channelIDServer.channelIDInster(temp);
    await updateDicts(name, false);
  } catch (error) {
    logger.error(`插入${toString(data)} 出错`);
    logger.error(`修改${toString(name)} 出错`);
  }
}
// 拉取然后修改两个库
function requestApi(data) {
  const key = getKey();
  API.search(data[data.type], key).then((res) => {
    const item = dataCheck(res);
    if (item) {
      updateData(item, data);
    } else {
      updateDicts(data, true);
    }
  }).catch(() => {
    // 查询出了错
  });
}
// 任务入口
const startJob = async () => {
  const res = await nameDictsServer.naemDictFindOne(); // 如果查询不到会
  if (res && res.type) {
    requestApi(res);
  } else {
    await cancel();
  }
};
// 取消任务
const cancel = async (ctx, next) => {
  if (JOB) {
    JOB.cancel();
    logger.info(`search任务已经取消：${JOB}`);
    JOB = null;
    if (ctx) {
      ctx.body = { success: `search任务取消-JOB：${JOB}` };
      await next();
    }
  } else {
    logger.error(`search任务还未开始-JOB：${JOB}`);
    if (ctx) {
      ctx.body = { error: `search任务还未开始-JOB：${JOB}` };
      await next();
    }
  }
};
// 启动任务
const scdule = async (ctx, next) => {
  if (!JOB) {
    const rule = new schedule.RecurrenceRule();
    rule.minute = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
    logger.info(`search任务启动-minute：${rule.minute}`);
    JOB = schedule.scheduleJob(rule, () => {
      startJob();
    });
    ctx.body = { success: `search任务启动-minute：${rule.minute}` };
    await next();
  } else {
    ctx.body = { error: 'search scdule is alerady exits' };
    logger.error('search scdule is alerady exits');
    await next();
  }
};

module.exports.cancel = async (ctx, next) => {
  await cancel(ctx, next);
};

module.exports.start = async (ctx, next) => {
  await scdule(ctx, next);
};
