const searchChannel = require('../Dao/searchChannelID');
const logger = require('../utill/logger.js');

function toString(obj) {
  return JSON.stringify(obj);
}

const insterDataPater = {
  level: 1,
  taskstatus: 'false',
};
//  修改将查找到的数据taskstatus 值都改为 success
module.exports.channelIDChangeStatus = async (findjson) => {
  await searchChannel.channelIDChangeStatus(findjson, { $set: { taskstatus: 'success', level: 0 } });
};

// 查找50  taskstatus = false 安装level 排序
module.exports.channelIDFind50 = async () => {
  const result = await searchChannel.channelIDFind50();
  if (result && result.length > 0) {
    return result;
  }
  return false;
};

// 插入对象
module.exports.channelIDInster = async (insterjson) => {
  const temp = { ...insterDataPater, ...insterjson };
  temp.spUtcDate = new Date().getTime();
  temp.spDate = new Date();
  if (temp.id.channelId) {
    temp.channelId = temp.id.channelId;
    await searchChannel.channelIDInster(temp);
  } else {
    logger.error(`传入数据错误:${toString(insterjson)}`);
  }
};
