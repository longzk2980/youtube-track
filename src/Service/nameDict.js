const nameDictDao = require('../Dao/nameDict.js');
const logger = require('../utill/logger.js');
const timingchannelId = require('../Dao/timingchannelID.js');
const searchChannel = require('../Service/searchChannel.js');

const insterDataPater = {
  keyword: null,
  channelUrl: null,
  channelId: null,
  name: null,
  platform: null,
  tags: ['视频'],
  taskstatus: 'false',
  level: 1,
  id: {
    channelId: null,
  },
};

function toString(obj) {
  return JSON.stringify(obj);
}
// 匹配出channel ID
function regxForUrl(url) {
  const regex = new RegExp(/http(s)?:\/\/(www|m).youtube.com\/((channel|c|user)\/)+([a-zA-Z0-9-_.]+)\/?(.*)/, 'g');
  return regex.exec(url);
}


// 插入数据 searchfrom = true 就是需要及时获取的数据 同时插入到 channel表跟 timming表
module.exports.naemDictInster = async (ctx, next) => {
  const postData = ctx.request.body;
  logger.info(`接收数据:${toString(postData)}`);
  const temp = { ...insterDataPater, ...postData };
  // 还需要判断 发送过来的数据。是否符合格式 不符合格式就返回提示 还需吧ID从URL里面取出来
  if (temp.channelUrl) {
    const channelIDlist = regxForUrl(temp.channelUrl);
    if (channelIDlist) {
      // eslint-disable-next-line prefer-destructuring
      temp.channelId = channelIDlist[5];
    } else {
      ctx.body = { error: 'error连接错误' };
      logger.error('链接错误');
      await next();
      return;
    }
  }
  if (temp.tags) {
    temp.tags = temp.tags.split(',');
  }
  if (temp.searchfrom && temp.channelId) {
    temp.level = 2;
    temp.id.channelId = temp.channelId;
    await timingchannelId.timingchannelInster(temp);
    await searchChannel.channelIDInster(temp);
  } else {
    await nameDictDao.naemDictInster(temp);
  }
  ctx.body = temp;
  await next();
};

// 找出一个使用then 拆包拆开   // 返回的值里面添加type让调用方好知道通过什么来搜索处理
module.exports.naemDictFindOne = async () => {
  const result = await nameDictDao.naemDictFindOne();
  if (result && result.length > 0) {
    const res = result[0];
    // eslint-disable-next-line no-nested-ternary
    res.type = res.name ? 'name' : res.keyword ? 'keyword' : res.channelUrl ? 'channelUrl' : false;
    return res;
  }
  return false;
};

// 改变状态 {name:'',channelID:'',taskstatus:'false'} 会将查出来的所有都变为 success
module.exports.naemDictChangeStatus = async (findjson, searchresult) => {
  await nameDictDao.naemDictChangeStatus(findjson, searchresult);
};
