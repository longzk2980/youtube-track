const videoListDao = require('../Dao/videoList');
const logger = require('../utill/logger.js');

function toString(obj) {
  return JSON.stringify(obj);
}

//  修改将查找到的数据taskstatus 值都改为 success
module.exports.VideoIDChangeStatus = async (findjson) => {
  await videoListDao.ChangeStatus(findjson);
};

// 查找50  taskstatus = false 安装level 排序
module.exports.VideoIDFind50 = async () => {
  const result = await videoListDao.Find50();
  if (result && result.length > 0) {
    const List = [];
    result.forEach((e) => {
      if (e.contentDetails.videoId) {
        List.push(e.contentDetails.videoId);
        logger.log(`查询的playitemsList里面的channelid:${e.channelId};videoid:${e.contentDetails.videoId};id:${e.id}`);
      }
    });
    return List;
  }
  return false;
};


//   FindById
module.exports.VideoFindById = async (channelId) => {
  const result = await videoListDao.FindById(channelId);
  if (result && result.length > 0) {
    const List = [];
    result.forEach((e) => {
      if (e.channelId) {
        List.push(e.channelId);
        logger.log(`查询的playitemsList里面的channelid:${e.channelId};videoid:${e.id};`);
      }
    });
    // 输出id
    return result;
  }
  return false;
};
// 插入对象
// 插入数据
module.exports.VideoInsterMany = async (jsonarry) => {
  if (jsonarry && jsonarry.length > 0) {
    const temp = [];
    jsonarry.forEach((el) => {
      const item = el;
      item.channelId = el.snippet.channelId;
      item.taskstatus = 'false';
      item.spUtcDate = new Date().getTime();
      item.spDate = new Date();
      temp.push(item);
    });
    await videoListDao.InsterMany(temp);
  } else {
    logger.error(`传入数据错误:${toString(jsonarry)}`);
  }
};
