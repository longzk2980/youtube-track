const channelDetailDao = require('../Dao/channeDetailList.js');
const logger = require('../utill/logger.js');

function toString(obj) {
  return JSON.stringify(obj);
}

// 插入数据
module.exports.channelListInsterMany = async (jsonarry, searchList) => {
  if (jsonarry && jsonarry.length > 0) {
    const temp = [];
    jsonarry.forEach((el) => {
      const item = el;
      item.channelId = el.id;
      item.taskstatus = 'false';
      item.spUtcDate = new Date().getTime();
      item.spDate = new Date();
      searchList.forEach((element) => {
        if (item.channelId === element.channelId) {
          item.level = element.level;
        }
      });
      temp.push(item);
    });
    await channelDetailDao.InsterMany(temp);
  } else {
    logger.error(`传入数据错误:${toString(jsonarry)}`);
  }
};

// 找出一个
module.exports.channelListFindOne = async () => {
  const result = await channelDetailDao.FindOne();
  if (result && result.length > 0) {
    const res = result[0];
    return res;
  }
  return false;
};

// 改变状态 {name:'',channelID:'',taskstatus:'false'} 会将查出来的所有都变为 success
module.exports.channelListChangeStatus = async (findjson) => {
  // { 'contentDetails.relatedPlaylists.uploads': Id ,taskstatus:false}
  await channelDetailDao.ChangeStatus(findjson);
};

// 查找功能

module.exports.Find = async (findjson) => {
  const result = await channelDetailDao.Find(findjson);
  if (result && result.length > 0) {
    const res = result[0];
    return res;
  }
  return false;
};
