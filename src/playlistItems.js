/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
/* eslint-disable prefer-promise-reject-errors */
const schedule = require('node-schedule');
const db = require('./utill/db.js');
const API = require('./api');
const API_KEY = require('./utill/apikey.js');


const dburl = 'mongodb://127.0.0.1:27017/';
// let dburl = "mongodb://192.168.1.27/";

// 查询出 1个
const findDictsOne = () => new Promise((resolve, reject) => {
  db.find(dburl, 'youtubeSpider', 'channelsList', { spStatus: 'false', 'contentDetails.relatedPlaylists.uploads': { $ne: '' } }, { pageamount: 1, page: 0, sort: { spUtcDate: 1 } }, (err, result) => {
    if (err) {
      console.log('channelsList数据查找失败！');
      db.close();
      return;
    }
    if (result.length > 0) {
      resolve(result);
    } else {
      reject({ err: 'alldone' });
    }
  });
});

// 将拉到到数据插入到channelList
function insertPlayItemsList(list, Iditem) {
  db.insertMany(dburl, 'youtubeSpider', 'playitemsList', list, (err, result) => {
    if (err) {
      console.log('playitemsList插入失败！');
      return;
    }
    console.log('playitemsList成功存库');
    updatePlaylistId(Iditem);
  });
}

// 查询插入完成后将拉取到到数据更新channlID 表
function updatePlaylistId(Id) {
  db.upsert(dburl, 'youtubeSpider', 'channelsList', { 'contentDetails.relatedPlaylists.uploads': Id }, { $set: { spStatus: 'success' } }, { multi: true }, (err, result) => {
    if (err) {
      console.log('channelsList数据修改失败！');
      return;
    }
    console.log(Id, 'channelsList修改成功');
  });
}

// 对返回到数据做检查和清理出错到话及时拦截停止
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data.length > 0) {
    const items = [];
    data.forEach((el) => {
      const item = el;
      item.channelId = el.snippet.channelId;
      item.spStatus = 'false';
      item.spUtcDate = new Date().getTime();
      item.spDate = new Date();
      items.push(item);
    });
    return items;
  } if (data.length === 0) {
    return false;
  }
  console.log('check 出错了:');
  cancelScd();
  return 'error';
}

// 通过 idList 发起请求
function requestApi(Id, key, pagetoken) {
  return new Promise((resolve, reject) => {
    API.playListItem(Id, key, pagetoken).then((res) => {
      const playlist = dataCheck(res);
      if (playlist) {
        console.log('返回结果正确进行数据库操作');
        insertPlayItemsList(playlist, Id);
      } else if (playlist === false) {
        console.log('返回结果正确但是是个空数组进行数据库更新操作');
        updatePlaylistId(Id);
      } else {
        console.log('返回结果错误');
        reject({ err: 'api查询出错' });
      }
      resolve(res);
    }).catch((error) => {
      console.log('请求结果错误', error);
      reject({ err: 'api查询出错' });
    });
  });
}

let PAGE_SUM = 0;

async function pageRequest(id, key, pagetoken) {
  const res = await requestApi(id, key, pagetoken);
  const nextRes = JSON.parse(res);
  console.log(`请求并插入库中的结果 etg: ===${nextRes.etag}`);
  if (nextRes.nextPageToken && PAGE_SUM < 7) {
    // 有下一页就取消定时任务 执行页面请求的回调，一直到没有next page在重启任务
    cancelScd();
    console.log('下一页pagetoken', nextRes.nextPageToken);
    setTimeout(() => {
      console.log('下一页执行,第几页', PAGE_SUM);
      PAGE_SUM += 1;
      pageRequest(id, key, nextRes.nextPageToken);
    }, 1000 * 60);
  } else {
    PAGE_SUM = 0;
    console.log('请求没有token: 第页', PAGE_SUM);
    Scdule();
  }
}

// 获取 Playid 列表 好变成字符串 然后放到 请求里面
function IdListClear(reslist) {
  const item = reslist[0];
  if (item.contentDetails.relatedPlaylists.uploads) {
    console.log(`uplistId:${item.contentDetails.relatedPlaylists.uploads};====id:${item.id}======channelId:${item.channelId}`);
    return item.contentDetails.relatedPlaylists.uploads;
  }
  return false;
}

// 启动抓取任务
const startInit = async () => {
  try {
    const key = API_KEY.API_KEY;
    console.log(`=======================schedule Cronstyle:${new Date()}======================`);
    const PlayListId = await findDictsOne();
    await pageRequest(IdListClear(PlayListId), key, '');
  } catch (error) {
    console.log('error:', error);
    if (error.err === 'alldone') {
      cancelScd();
    }
  }
};

// 取消任务
let job = null;
const cancelScd = () => {
  if (job) {
    job.cancel();
    console.log('getPlayListItems schedule is cancel');
    job = null;
  } else {
    console.log('getPlayListItems schedule is not defin');
  }
};

// 任务定时
const Scdule = async () => {
  if (!job) {
    const rule = new schedule.RecurrenceRule();
    // rule.dayOfWeek = 2;
    // rule.month = 3;
    // rule.dayOfMonth = 1;
    // rule.hour = 1;
    // rule.minute = [0,20,40];
    rule.minute = [4, 9, 14, 19, 24, 29, 34, 39, 44, 49, 54, 59];
    // const second = parseInt(30)
    // rule.second = [0,5,10,15,20,25,30,35,40,45,50,55,60]
    console.log(rule, '定时时间');
    job = schedule.scheduleJob(rule, () => {
      startInit();
    });
  } else {
    console.log('getPlayListItems schedule is alerady exits');
  }
};

module.exports.cancelPlayListItems = async () => {
  cancelScd();
};
module.exports.getPlayListItems = async () => {
  Scdule();
};

// TODO: 通过 channelsList 里面的upload 列表 通过playlistItems 接口获取 视频列表

// TODO: 然后将视频列表里面的视频ID 组队，通过video 获取视频信息
