/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-unreachable */
/* eslint-disable no-console */
const Crawler = require('crawler');
const schedule = require('node-schedule');
const db = require('./utill/db.js');
const API = require('./api.js');

const dburl = 'mongodb://127.0.0.1:27017/';
// let dburl = "mongodb://192.168.1.27/";

/*
const c = new Crawler({
  // 在每个请求处理完毕后将调用此回调函数
  rateLimit: 1000 * 80,
  maxConnections: 1,
  headers:
    {
      'cache-control': 'no-cache',
      Connection: 'keep-alive',
    },
  jQuery: false,
  callback(error, res, done) {
    if (error) {
      console.log('爬虫error：', error);
    } else {
      try {
        const item = dataCheck(res);
        if (item) {
          updateData(item, res.options.name);
        } else {
          updateDicts(res.options.name);
        }
      } catch (error) {
        console.log(res.body);
        console.log(error);
      }
    }
    done();
  },
});
*/

function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data.length > 0) {
    const item = data[0];
    console.log(item);
    item.channelId = item.id.channelId;
    item.status = 'false';
    return item;
  }
  return false;
  console.log('check 出错了');
}

async function updateData(data, name) {
  db.upsert(dburl, 'youtubeSpider', 'ChannelID', { channelId: data.channelId }, { $set: data }, { upsert: true }, (err, result) => {
    if (err) {
      console.log('数据修改失败！');
      return;
    }
    console.log('抓取成功存库');
    updateDicts(name);
  });
}

async function updateDicts(name) {
  db.upsert(dburl, 'youtubeSpider', 'nameDicts', { name }, { $set: { name, taskstatus: 'success' } }, { upsert: true }, (err, result) => {
    if (err) {
      console.log('数据修改失败！');
      return;
    }
    console.log(name, '字典修改成功');
  });
}

const findDicts = () => new Promise((resolve, reject) => {
  db.find(dburl, 'youtubeSpider', 'nameDicts', { taskstatus: 'false' }, (err, result) => {
    if (err) {
      console.log('数据查找失败！');
      db.close();
      return;
    }
    resolve(result);
  });
});

const findDictsOne = () => new Promise((resolve, reject) => {
  db.find(dburl, 'youtubeSpider', 'nameDicts', { taskstatus: 'false' }, { pageamount: 1, page: 1, sort: {} }, (err, result) => {
    if (err) {
      console.log('数据查找失败！');
      db.close();
      return;
    }
    if (result.length > 0) {
      resolve(result);
    } else {
      reject({ err: 'alldone' });
    }
  });
});

const requestApi = async (name, key) => new Promise((resolve, reject) => {
  const data = {
    q: name,
    key,
  };
  API.getApi(data).then((res) => {
    console.log(res, '请求结果');
    const item = dataCheck(res);
    if (item) {
      updateData(item, name);
    } else {
      updateDicts(name);
    }
    resolve('success');
  }).catch((error) => {
    reject({ err: 'api查询出错' });
  });
});

let job = null;
const cancelScd = () => {
  if (job) {
    job.cancel();
    console.log('schedule is cancel');
    job = null;
  } else {
    console.log('schedule is not defin');
  }
};

const startDicts = async () => {
  try {
    const key = 'AIzaSyBqj9nCzVH7XuzulcdHSGJYp3NqSi8bVs0';
    console.log(`scheduleCronstyle:${new Date()}`);
    const url = await findDictsOne();
    const res = requestApi(url[0].name, key);
  } catch (error) {
    console.log('error:', error);
    if (error.err === 'alldone') {
      cancelScd();
    }
  }
};

const Scdule = async () => {
  if (!job) {
    const rule = new schedule.RecurrenceRule();
    // rule.dayOfWeek = 2;
    // rule.month = 3;
    // rule.dayOfMonth = 1;
    // rule.hour = 1;
    // rule.minute = [0,20,40];
    rule.minute = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
    // const second = parseInt(30)
    // rule.second = [0,5,10,15,20,25,30,35,40,45,50,55,60]

    console.log(rule, '定时时间');
    job = schedule.scheduleJob(rule, () => {
      startDicts();
    });
  } else {
    console.log('schedule is alerady exits');
  }
};


module.exports.cancelChannel = async () => {
  cancelScd();
};

module.exports.requestChannel = async () => {
  Scdule();
};


module.exports.searchChannel = async () => {
  const res = await findDicts();
  const urls = [];
  res.forEach((el) => {
    const url = {
      // url: `https://www.baidu.com/`,
      url: `https://www.googleapis.com/youtube/v3/search?part=id,snippet&key=${key}&type=channel&q=${encodeURIComponent(el.name)}&maxResults=1&order=viewCount`,
      name: el.name,
    };
    urls.push(url);
  });
  console.log(urls);
  // c.queue(urls);
  // c.queue([urls[0]])
};

module.exports.channelId = async (id) => {
  console.log(id);
  // c.queue([{
  //     url:`https://www.googleapis.com/youtube/v3/search?part=id,snippet&key=${key}&type=channel&q=${encodeURIComponent(el.name)}&maxResults=1&order=viewCount`,
  //     rateLimit: Math.random()
  // }])
};
