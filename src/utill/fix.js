// FIXME: 这是一些例子

const Crawler = require('crawler');


const tempres = [];
const c = new Crawler({
  // 在每个请求处理完毕后将调用此回调函数
  callback(error, res, done) {
    if (error) {
      console.log(error);
    } else {
      const { $ } = res;
      console.log($('title').text(), '获取到到值');
      tempres.forEach((el, index) => {
        if (el.vId === res.options.vId) {
          tempres[index].contxt = $('title').text();
        }
      });
    }
    done();
  },
});

const checkTemp = (vId) => new Promise((resolve, reject) => {
  const check = () => {
    tempres.forEach((el) => {
      if (el.vId === vId) {
        if (el.contxt) {
          resolve(tempres);
        } else {
          setTimeout(() => {
            check();
          }, 1000 * 1);
        }
      }
    });
  };
  check();
  setTimeout(() => {
    reject('');
  }, 1000 * 10);
});

const getvideo = async (ctx) => {
  const url = ctx.request.query.url + Math.random() * 10;
  const vId = ctx.request.query.vId + Math.random() * 10;
  // 将一段HTML代码加入请求队列，即不通过抓取，直接交由回调函数处理（可用于单元测试）
  tempres.push({
    vId,
    contxt: null,
  });
  c.queue([{
    html: `<title>${url}</title>`,
    vId,
  }]);
  const data = await checkTemp(vId);
  ctx.response.status = 200;
  if (data.length > 1) data.shift();
  console.log(data, '返回值');
  ctx.response.body = data;
};
