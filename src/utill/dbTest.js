/* eslint-disable no-undef */
/* eslint-disable no-console */

db.insertOne('MissionTasks', 'student', { name: 'qianqiang' }, (err, result) => {
  if (err) {
    console.log('数据插入失败！');
    db.close();
    return;
  }
  console.log(result);
});
db.deleteMany('MissionTasks', 'student', { age: 11 }, (err, result) => {
  if (err) {
    console.log('数据删除失败！');
    db.close();
    return;
  }
  console.log(result);
});

// 如果没有就插入
db.upsert('MissionTasks', 'student', { age: 18 }, { $set: { age: 25 } }, true, (err, result) => {
  if (err) {
    console.log('数据库修改失败！');
    return;
  }
  console.log(result);
});

db.updateMany('MissionTasks', 'student', { age: 18 }, { $set: { age: 25 } }, (err, result) => {
  if (err) {
    console.log('数据修改失败！');
    db.close();
    return;
  }
  console.log(result);
});
db.find('MissionTasks', 'student', {}, { pageamount: 2, page: 4, sort: {} }, (err, result) => {
  if (err) {
    console.log('数据查找失败！');
    db.close();
    return;
  }
  console.log(result);
});
db.find('MissionTasks', 'student', {}, (err, result) => {
  if (err) {
    console.log('数据查找失败！');
    db.close();
    return;
  }
  console.log(result);
});
db.getAllCount('MissionTasks', 'student', (count) => {
  console.log(count);
});
