/* eslint-disable no-console */
const schedule = require('node-schedule');
const API = require('../api.js');

const API_KEY_LIST = [
  {
    keys: 'AIzaSyBc3xAnnqvIEh3pbdBQTm97Lpm7Ae29EEk',
    status: true,
    name: 'API Key 1',
  },
  {
    keys: 'AIzaSyD07YO3AAZS4yz9ecSBSDuz_VTuT4FBqyU',
    status: false,
    name: 'API Key 2',
  },
  {
    keys: 'AIzaSyD8jkG6-fnXxdSLHsvGaJkuTV1ql8USvs0',
    status: false,
    name: 'API Key 3',
  },
  {
    keys: 'AIzaSyAbGg-4hnGt1drjm_VmfzCR224t9FedUrI',
    status: false,
    name: 'API Key 4',
  },
  {
    keys: 'AIzaSyCQWIVVwdZNODEHszCdrSbputY0gI5-TA0',
    status: false,
    name: 'API Key 5',
  },
  {
    keys: 'AIzaSyDDXEXbIXdjDbAjMrFBBDrBb2wc_LSqBJg',
    status: false,
    name: 'API Key 6',
  },
  {
    keys: 'AIzaSyBj4mdtLRr-Oo6epzh8eyyIkziraKxpYHs',
    status: false,
    name: 'API Key 7',
  },
  {
    keys: 'AIzaSyAqnd3s_Uo_PDi5otqz5migp-QwIjCMWXk',
    status: false,
    name: 'API Key 8',
  },
  {
    keys: 'AIzaSyDPUX57TtssCtLXgeqYuxVUmz9skeiVEFI',
    status: false,
    name: 'API Key 9',
  },
];


function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data.length > 0) {
    return true;
  }
  return false;
}

let CURRENT_KEY = API_KEY_LIST[0].keys;


function googleApiTest(index) {
  setTimeout(() => {
    API.getApi({
      q: '滇西小哥',
      key: API_KEY_LIST[index].keys,
    }).then((res) => {
      dataCheck(res);
      CURRENT_KEY = API_KEY_LIST[index].keys;
    }).catch(() => {
      API_KEY_LIST[index].status = false;
    });
  }, 1000 * 60 * index);
}
// 取消任务
function testApiKey() {
  API_KEY_LIST.forEach((index) => {
    googleApiTest(index);
  });
}
// 取消任务
let job = null;
const cancelScd = () => {
  if (job) {
    job.cancel();
    console.log('getPlayListItems schedule is cancel');
    job = null;
  } else {
    console.log('getPlayListItems schedule is not defin');
  }
};
// 任务定时
const Scdule = async () => {
  if (!job) {
    const rule = new schedule.RecurrenceRule();
    // rule.dayOfWeek = 2;
    // rule.month = 3;
    // rule.dayOfMonth = 1;
    rule.hour = 23;
    // rule.minute = [0,20,40];
    // rule.minute = [4, 9, 14, 19, 24, 29, 34, 39, 44, 49, 54, 59];
    // const second = parseInt(30)
    // rule.second = [0,5,10,15,20,25,30,35,40,45,50,55,60]
    console.log(rule.hour, '定时时间');
    job = schedule.scheduleJob(rule, () => {
      testApiKey();
    });
  } else {
    console.log('定时检查key schedule is alerady exits');
  }
};

module.exports.cancelcheckKey = async () => {
  cancelScd();
};
module.exports.checkKey = async () => {
  Scdule();
};

module.exports.getCurrentKey = (index) => {
  const temp = index || 0;
  return API_KEY_LIST[temp].keys;
};

module.exports.API_KEY = CURRENT_KEY;

module.exports.API_KEY_VIDEOS = '';

module.exports.TDM_DB_LOCAL = 'mongodb://192.168.1.27/';

module.exports.TDM_DB_ONLINE = 'mongodb://2o5572137z.qicp.vip:31754/';

module.exports.AWS_DB = 'mongodb://127.0.0.1:27017/';
