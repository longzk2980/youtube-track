/* eslint-disable no-shadow */
/* eslint-disable no-unreachable */
/* eslint-disable no-redeclare */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable no-console */
/* eslint-disable no-underscore-dangle */
const { MongoClient } = require('mongodb');
// 连接数据库操作
function _connectDB(dburl, database, callback) {
  // var dburl = dburl || "mongodb://192.168.1.27/";
  MongoClient.connect(`${dburl}${database}`, (err, db) => {
    callback(err, db);
  });
}
// 插入函数的封装
module.exports.insertOne = (dburl, database, collection, json, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      return;
    }
    db.collection(collection).insertOne(json, (err, result) => {
      callback(err, result);
      db.close();
    });
  });
};
// 插入集合
module.exports.insertMany = (dburl, database, collection, arry, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库链接失败');
    }
    db.collection(collection).insertMany(arry, (err, result) => {
      callback(err, result);
      db.close();
    });
  });
};

// 删除函数的封装
module.exports.deleteMany = (dburl, database, collection, json, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      return;
    }
    db.collection(collection).deleteMany(json, (err, result) => {
      callback(err, result);
      db.close();
    });
  });
};
// 如果修改更新一个
module.exports.upsert = (dburl, database, collection, findjson, setjson, upsert, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      db.close();
      return;
    }
    db.collection(collection).update(findjson, setjson, upsert, (err, result) => {
      callback(err, result);
      db.close();
    });
  });
};
// 修改函数的封装
module.exports.updateMany = (dburl, database, collection, json1, json2, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      return;
    }
    db.collection(collection).updateMany(json1, json2, (err, result) => {
      callback(err, result);
      db.close();
    });
  });
};
// 获取集合当中文档的总条数
module.exports.getAllCount = (dburl, database, collection, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      return;
    }
    db.collection(collection).count({}).then((count) => {
      callback(count);
      db.close();
    });
  });
};
// 查找函数的封装
module.exports.find = function (dburl, database, collection, json, C, D) {
  if (arguments.length == 5) {
    var callback = C;
    var skipnumber = 0;
    var limit = 0;
    var sort = {};
  } else if (arguments.length == 6) {
    var callback = D;
    var args = C;
    var skipnumber = args.pageamount * args.page;
    var limit = args.pageamount;
    var { sort } = args;
  } else {
    throw new Error('find函数参数个数不正确！');
    return;
  }
  var result = [];
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      return;
    }
    var cursor = db.collection(collection).find(json).sort(sort).limit(limit)
      .skip(skipnumber);
    cursor.each((err, doc) => {
      if (err) {
        callback(err, null);
        db.close();
        return;
      }
      if (doc != null) {
        result.push(doc);
      } else {
        callback(null, result);
        db.close();
      }
    });
  });
};

// 查找重复
const aggregated = (dburl, database, collection, json, callback) => {
  _connectDB(dburl, database, (err, db) => {
    if (err) {
      console.log('数据库连接失败！');
      return;
    }
    db.collection(collection).aggregate(json, (err, result) => {
      callback(err, result);
      db.close();
    });
  });
};

// aggregated('mongodb://127.0.0.1:27017/', 'youtubeSpider', 'videosList', [
//   { $group: { _id: { id: '$id' }, channelId: { $push: '$channelId' }, count: { $sum: 1 } } },
//   { $match: { count: { $gt: 1 } } },
// ], (err, result) => {
//   console.log(result);
// });


// this.find('mongodb://127.0.0.1:27017/', 'youtubeSpider', 'playitemsList', {}, (err, result) => {
//   if (err) {
//     console.log('数据查找失败！');
//     db.close();
//     return;
//   }
//   if (result.length > 0) {
//     result.forEach((item) => {
//       this.find('mongodb://127.0.0.1:27017/', 'youtubeSpider', 'videosList', { id: item.contentDetails.videoId, channelId: item.channelId }, (err, result) => {
//         if (err) {
//           console.log('查询出错');
//         }
//         if (result.length === 0) {
//           console.log(item);
//         }
//       });
//     });
//   }
// });

// TODO: 使用aggreate 测试去重，将数据迁移到 机房

//
