const request = require('request');
const logger = require('./utill/logger.js');

function toString(obj) {
  return JSON.stringify(obj);
}
// 通用配置
function getOption(type, qs) {
  return {
    method: 'GET',
    url: `https://www.googleapis.com/youtube/v3/${type}`,
    qs,
    timeout: 1000 * 30,
    headers:
    {
      'cache-control': 'no-cache',
    },
  };
}

// search config
function getSearchOpt(q, key) {
  const type = 'search';
  const qs = {
    part: 'id,snippet',
    key,
    type: 'channel',
    q,
    maxResults: '1',
    // order: 'viewCount',
  };
  return getOption(type, qs);
}

// channel config
function getChannelOpt(q, key) {
  const type = 'channels';
  const qs = {
    part: 'brandingSettings ,contentDetails, contentOwnerDetails,id, invideoPromotion,localizations,snippet, statistics, status, topicDetails',
    key,
    id: q,
  };
  return getOption(type, qs);
}

// playlist 有多少个自目录
function getPlayListItemOpt(playlistId, key, pagetoken) {
  const type = 'playlistItems';
  const qs = {
    part: 'contentDetails,status,snippet,id',
    maxResults: '50',
    playlistId,
    key,
  };
  if (pagetoken) {
    qs.pageToken = pagetoken;
  }
  return getOption(type, qs);
}

// videos ID
function getVideoListOpt(q, key) {
  const type = 'videos';
  const qs = {
    part: 'contentDetails, snippet, localizations, statistics, status, topicDetails',
    key,
    id: q,
  };
  return getOption(type, qs);
}


// 获取channel 的详细信息 包括播放列表
function search(q, key) {
  return new Promise((resolve, reject) => {
    const opt = getSearchOpt(q, key);
    logger.info(`请求：${toString(opt)}`);
    request(opt, (error, response, body) => {
      if (error) {
        logger.error(`查询出错-配置${toString(opt)}`);
        reject();
      }

      resolve(body);
    });
  });
}

// 获取channel 的详细信息 包括播放列表
function channels(channelID, key) {
  return new Promise((resolve, reject) => {
    const opt = getChannelOpt(channelID, key);
    logger.info(`请求：${toString(opt)}`);
    request(opt, (error, response, body) => {
      if (error) {
        logger.error(`查询出错-配置${toString(opt)}`);
        reject();
      }
      resolve(body);
    });
  });
}

// 获取播放列表里面的视频ID
function playListItem(playlistId, key, pageToken) {
  return new Promise((resolve, reject) => {
    const opt = getPlayListItemOpt(playlistId, key, pageToken);
    logger.info(`请求：${toString(opt)}`);
    request(opt, (error, response, body) => {
      if (error) {
        logger.error(`查询出错-配置${toString(opt)}`);
        reject();
      }
      resolve(body);
    });
  });
}

// 拉取视频播放信息
function videoList(videoID, key) {
  return new Promise((resolve, reject) => {
    const opt = getVideoListOpt(videoID, key);
    logger.info(`请求：${toString(opt)}`);
    request(opt, (error, response, body) => {
      if (error) {
        logger.error(`查询出错-配置${toString(opt)}`);
        reject();
      }
      resolve(body);
    });
  });
}

// TODO: 发送数据的URL还没有确定
// 发送数据的配置
function sendOption(data) {
  return {
    method: 'POST',
    url: '',
    data,
    timeout: 1000 * 30,
    headers:
    {
      'cache-control': 'no-cache',
    },
  };
}
// 发送频道数
function sendChannel(data) {
  return new Promise((resolve, reject) => {
    const opt = sendOption(data);
    logger.info(`请求：${toString(opt)}`);
    request(opt, (error, response, body) => {
      if (error) {
        logger.error(`查询出错-配置${toString(opt)}`);
        reject();
      }
      resolve(body);
    });
  });
}

// 发送Video数据
function sendVideo(data) {
  return new Promise((resolve, reject) => {
    const opt = sendOption(data);
    logger.info(`请求：${toString(opt)}`);
    request(opt, (error, response, body) => {
      if (error) {
        logger.error(`查询出错-配置${toString(opt)}`);
        reject();
      }
      resolve(body);
    });
  });
}


module.exports = {
  search,
  channels,
  playListItem,
  videoList,
  sendChannel,
  sendVideo,
};
