const Crawler = require('crawler');
const db = require('./utill/db.js');
const axios = require('axios');

const c = new Crawler({
    // 在每个请求处理完毕后将调用此回调函数
    headers:
    {
        'cache-control': 'no-cache',
        Connection: 'keep-alive',
        Cookie: 'connect.sid=s%3Anc1hevd-jZrXOC8CIObZUa-jXykQmjJx.HA726ESr4FATXnhgb1qI2APUiSxbivALF4mplvtO0iI; navigate-jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJ1c2VySWQiOiI1ZDg4ODQ4YzUyNzc2OTAwMTI1NTAzZTYiLCJpYXQiOjE1NjkyMjc5NjMsImV4cCI6MTU3MTgxOTk2MywiYXVkIjoiaHR0cHM6Ly9uYXZpZ2F0ZS5uZXR3b3JrIiwiaXNzIjoibmF2aWdhdGUiLCJzdWIiOiJhbm9ueW1vdXMiLCJqdGkiOiIxZDRkNzJhZi04NTAzLTRlNGQtODVjYi02ZjA3MjY1ZDIwYjQifQ.EcZaKLDPnNkm3FC4hBI2_YWGXNzCGK-wefUXcRG_oHA',
        'Accept-Encoding': 'gzip, deflate',
        Host: 'api-dev.navigate.network',
        'Postman-Token': '5621f58d-c758-4482-9862-057b8fd80323,289840c5-7e6b-4cf3-af18-d940a56207dd',
        'Cache-Control': 'no-cache',
        Accept: '*/*',
        'User-Agent': 'PostmanRuntime/7.17.1',
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJ1c2VySWQiOiI1ZDg4ODQ4YzUyNzc2OTAwMTI1NTAzZTYiLCJpYXQiOjE1NjkyMjc5NjMsImV4cCI6MTU3MTgxOTk2MywiYXVkIjoiaHR0cHM6Ly9uYXZpZ2F0ZS5uZXR3b3JrIiwiaXNzIjoibmF2aWdhdGUiLCJzdWIiOiJhbm9ueW1vdXMiLCJqdGkiOiIxZDRkNzJhZi04NTAzLTRlNGQtODVjYi02ZjA3MjY1ZDIwYjQifQ.EcZaKLDPnNkm3FC4hBI2_YWGXNzCGK-wefUXcRG_oHA',
        'Content-Type': 'application/json'
    },
    jQuery: false,
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            const data = JSON.parse(res.body).data
            instData(data)
        }
        done();
    }
});

let flag = 0
async function instData(data) {
    data.forEach((element, index) => {
        if (element.tags.indexOf('视频') !== -1 || element.tags.indexOf('媒体') !== -1 || element.tags.indexOf('电影') !== -1) {
            flag++
            console.log(flag, element.displayName, element.tags)
            db.upsert('youtubeSpider', 'nameDicts',{'name':element.displayName},{$set:{ 'name': element.displayName, 'platform': 'weibo', 'tags': element.tags ,taskstatus:'success'}}, {upsert:true}, (err, result) => {
                if (err) {
                    console.log('数据库修改失败！');
                    return;
                }
                console.log('数据库修成功')
            });
        }
    });
}


module.exports.getChannelID = (num  =10000 ) => {
    const ruls = []
    for (let i = 1; i <= (num / 200); i++) {
        let url = {
            url: `https://api-dev.navigate.network/influencers/?platform=weibo&$skip=${i * 200}&$limit=${(i + 1) * 200}`,
            rateLimit: Math.random()
        }
        ruls.push(url)
    }
    // console.log(ruls)
    c.queue(ruls[0]);
}
