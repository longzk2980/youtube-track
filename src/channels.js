/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
/* eslint-disable prefer-promise-reject-errors */
const schedule = require('node-schedule');
const db = require('./utill/db.js');
const API = require('./api');
const KEY = require('./utill/apikey');


const dburl = KEY.AWS_DB;
// let dburl = KEY.TDM_DB_LOCAL;

// 查询出 50个一批
function find50Dict() {
  return new Promise((resolve, reject) => {
    db.find(dburl, 'youtubeSpider', 'ChannelID', { status: 'false' }, { pageamount: 50, page: 0, sort: {} }, (err, result) => {
      if (err) {
        console.log('数据查找失败！');
        db.close();
        return;
      }
      if (result.length > 0) {
        resolve(result);
      } else {
        reject({ err: 'alldone' });
      }
    });
  });
}

// 将拉到到数据插入到channelList
function insertChannelsList(Channellist, Idlist) {
  db.insertMany(dburl, 'youtubeSpider', 'channelsList', Channellist, (err, result) => {
    if (err) {
      console.log('数据插入失败！');
      return;
    }
    console.log('成功存库');
    updateChannelId(Idlist);
  });
}

// 查询插入完成后将拉取到到数据更新channlID 表
function updateChannelId(Idlist) {
  Idlist.forEach((item) => {
    db.upsert(dburl, 'youtubeSpider', 'ChannelID', { channelId: item }, { $set: { status: 'success' } }, { upsert: false }, (err, result) => {
      if (err) {
        console.log('数据修改失败！');
        return;
      }
      console.log(item, '字典修改成功');
    });
  });
}

// 对返回到数据做检查和清理出错到话及时拦截停止
function dataCheck(res) {
  const data = JSON.parse(res).items;
  if (data.length > 0) {
    const items = [];
    data.forEach((el) => {
      const item = el;
      item.channelId = el.id;
      item.spStatus = 'false';
      item.spUtcDate = new Date().getTime();
      item.spDate = new Date();
      items.push(item);
    });
    return items;
  } if (data.length === 0) {
    return false;
  }
  console.log('check 出错了:', res);
  cancelScd();
  return 'error';
}

// 通过 idList 发起请求
function requestApi(Idlist, key) {
  return new Promise((resolve, reject) => {
    API.channels(Idlist.toString(), key).then((res) => {
      const Channellist = dataCheck(res);
      if (Channellist) {
        insertChannelsList(Channellist, Idlist);
      } else if (Channellist === false) {
        updateChannelId(Idlist);
      } else {
        console.log('返回值：', res);
        reject({ err: 'api查询出错' });
      }
      resolve('success');
    }).catch((error) => {
      console.log(error);
      reject({ err: 'api查询出错' });
    });
  });
}

// 获取 id 列表 好变成字符串 然后放到 请求里面
function IdListClear(reslist) {
  const list = [];
  reslist.forEach((el) => {
    list.push(el.channelId);
  });
  return list;
}

// 启动抓取任务
const startInit = async () => {
  try {
    const key = KEY.API_KEY;
    console.log(`schedule Cronstyle:${new Date()}`);
    const Idlist = await find50Dict();
    console.log('查询ID', IdListClear(Idlist));
    await requestApi(IdListClear(Idlist), key);
  } catch (error) {
    console.log('error:', error);
    if (error.err === 'alldone') {
      cancelScd();
    }
  }
};

// 取消任务
let job = null;
const cancelScd = () => {
  if (job) {
    job.cancel();
    console.log('channelsList schedule is cancel');
    job = null;
  } else {
    console.log('channelsList schedule is not defin');
  }
};

// 任务定时
const Scdule = async () => {
  if (!job) {
    const rule = new schedule.RecurrenceRule();
    // rule.dayOfWeek = 2;
    // rule.month = 3;
    // rule.dayOfMonth = 1;
    // rule.hour = 1;
    // rule.minute = [0,20,40];
    rule.minute = [3, 8, 13, 18, 23, 28, 33, 38, 43, 48, 53, 58];
    // const second = parseInt(30)
    // rule.second = [0,5,10,15,20,25,30,35,40,45,50,55,60]
    console.log(rule, '定时时间');
    job = schedule.scheduleJob(rule, () => {
      startInit();
    });
  } else {
    console.log('channelsList schedule is alerady exits');
  }
};

module.exports.cancelChannelsList = async () => {
  cancelScd();
};
module.exports.getChannelsList = async () => {
  Scdule();
};
