{
    "restartable": "rs",
    "verbose": false,
    "env": {
        "NODE_ENV": "development", 
        "PORT": "39005" 
    },   
    "execMap": {
       
        "": "node --debug", 
       
        "js": "node --debug"
    },   
    "watch": [
      "*"
    ],
    "ignore": [
        ".git",
        ".idea",
        "node_modules"
    ],
    "ext": "js jade"
}